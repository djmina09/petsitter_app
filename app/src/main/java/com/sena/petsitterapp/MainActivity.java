 package com.sena.petsitterapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


 public class MainActivity extends AppCompatActivity {
     private Button btnIngresar;
     private Button btnCuidador;
     private Button btnDue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnIngresar = findViewById(R.id.btnIngresar);
        btnCuidador = findViewById(R.id.btnCuidador);
        btnDue = findViewById(R.id.btnDue);



    }

     public void pasar(View view) {
         Intent intent = new Intent(MainActivity.this, registroCuidador.class);
         startActivity(intent);

     }

     public void pasar1(View view) {
         Intent intent = new Intent(MainActivity.this, Ingresar.class);
         startActivity(intent);

     }

     public void pasar2(View view) {
         Intent intent = new Intent(MainActivity.this, RegistroDue.class);
         startActivity(intent);

     }
 }
