package com.sena.petsitterapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.sena.petsitterapp.modelos.Usuario;

public class RegistroDue extends AppCompatActivity {

    private EditText editTextNombre;
    private EditText editTextTelefono;
    private EditText editTextDocumento;
    private EditText editTextCorreo;
    private EditText editTextDireccion;
    private EditText editTextPassword;

    private FirebaseAuth mAuth;
    private FirebaseDatabase database;
    private DatabaseReference reference;
    private Usuario usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_due);

        editTextNombre = findViewById(R.id.txtNombre);
        editTextTelefono = findViewById(R.id.txtTelefono);
        editTextDocumento = findViewById(R.id.txtDocumento);
        editTextCorreo = findViewById(R.id.txtCorreo);
        editTextDireccion = findViewById(R.id.txtDirrecion);
        editTextPassword = findViewById(R.id.txtPassword);

        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        reference = database.getReference("usuarios");

    }

    public void registroOwner(View view) {

        usuario = new Usuario();
        usuario.setNombre(editTextNombre.getText().toString());
        usuario.setTelefono(editTextTelefono.getText().toString());
        usuario.setNumeroDocumento(editTextDocumento.getText().toString());
        usuario.setCorreo(editTextCorreo.getText().toString());
        usuario.setDireccion(editTextDireccion.getText().toString());
        usuario.setPassword(editTextPassword.getText().toString());
        usuario.setTipoUsuario("Dueño");

        mAuth.createUserWithEmailAndPassword(usuario.getCorreo(),usuario.getPassword()).addOnCompleteListener(RegistroDue.this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    FirebaseUser user = mAuth.getCurrentUser();
                    usuario.setIdUsuario(user.getUid());

                    reference.child("Dueños").child(usuario.getIdUsuario()).setValue(usuario);
                }else{
                    Toast.makeText(RegistroDue.this, "No se puedo registrar el dueño",Toast.LENGTH_LONG).show();
                }
            }
        });

    }
}
